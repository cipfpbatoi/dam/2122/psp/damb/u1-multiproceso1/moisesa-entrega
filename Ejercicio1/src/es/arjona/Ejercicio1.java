package es.arjona;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {

    static final int WAIT_TIME = 2;


    final static String OUTPUT_FILE_NAME = "output.txt";


    public static void main(String[] args) {
        if (args.length <= 0) {
            System.err.println("No se ha pasado ningun comando");
            System.exit(-1);
        }

        List command = new ArrayList<>();

        String osName = System.getProperty("os.name") + " Version: " + System.getProperty("os.version");
        if (osName.contains("Windows")) {
            command.add("cmd");
            command.add("/c");
        }

        command.addAll(Arrays.asList(args));

        try {
            Process process = new ProcessBuilder(command).start();

            if (!process.waitFor(WAIT_TIME, TimeUnit.SECONDS)){
                //Siempre llaves, aunque sea sólo una sea sólo una línea
                System.err.println("Agotado el tiempo de espera");
            }
            else {
                System.out.println("Ejecutando: " + command.toString());
                if (process.exitValue() == 0) {
                    printAndSaveChildOutput(process);
                } else
                System.err.println("Error al ejecutar el proceso hijo");
                showChildError(process);
                System.exit(-1);
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private static void showChildError(Process process) {
        String line;
        /**
         * La salida por la que se muestra el error es:
         * 
         * process.getErrorStream() como no está la salida estándar redirigida,
         * la mostrará por ahí.
         * 
         */
        try(BufferedReader bf = new BufferedReader(new InputStreamReader(process.getErrorStream()));) {
            while ((line = bf.readLine()) != null) {
                System.err.println(line);
            }
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
        }
    }


    private static void printAndSaveChildOutput(Process process) {
        String line;
        /**
         * Muy bien utilizando esta sintaxis. Tengo que actualizar los apuntes.
         * 
         */
        try(BufferedReader bf = new BufferedReader(new InputStreamReader(process.getInputStream()));
            FileWriter fw = new FileWriter(OUTPUT_FILE_NAME);) {
            while ((line = bf.readLine()) != null) {
                System.out.println(line);
                fw.write(line + System.lineSeparator());
                /**
                 * Ideal utilizar un BufferedWriter 
                 * para poder utilizar newLine en lugar
                 * de System.lineSeparator que aunque esté bien,
                 * según la documentación es mejor utilizar el otro.
                 */
            }
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
        }
        System.out.println("La ejecución del proceso hijo se ha guardado en el fichero: " + OUTPUT_FILE_NAME);
    }

}
