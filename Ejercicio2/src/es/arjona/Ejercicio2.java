package es.arjona;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio2 {

    final static String RANDOM_FILE = "random10.txt";

    public static void main(String[] args) {

        final String random10Path = "out/artifacts/Random10_jar/Random10.jar";

        Process random10 = null;
        try {
            random10 = new ProcessBuilder("java", "-jar", random10Path).start();
            System.out.println("Proceso hijo iniciado");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(random10.getOutputStream());
            InputStreamReader inputStreamReader = new InputStreamReader(random10.getInputStream());

            try (BufferedWriter bufWriter = new BufferedWriter(outputStreamWriter);
                 BufferedReader bufReader = new BufferedReader(inputStreamReader);

                 FileWriter fw = new FileWriter(RANDOM_FILE)) {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                String input;
                boolean goOn;
                do {
                    System.out.println("Escribe cualquier palabara para recibir un numero aleatorio del 1 al 10, con stop se para el programa");
                    input = br.readLine();

                    bufWriter.write(input);
                    bufWriter.newLine();
                    bufWriter.flush();

                    goOn = !input.equalsIgnoreCase("stop");
                    if (goOn) {
                        String number = bufReader.readLine();
                        System.out.println(number);
                        fw.write(number + System.lineSeparator());
                        /**
                         * Mejor utilizar un buffered writer para 
                         * no tener que poner lineSeparator.
                         */
                    }
                } while (goOn);


            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        } finally {
            int exitValue = 0;
            try {
                exitValue = random10.waitFor();
            } catch (InterruptedException e) {
                System.err.println("InterruptedException: " + e.getMessage());
                System.exit(3);
            }
            System.out.println(System.lineSeparator() + "La ejecución del programa Random10 devuelve el valor de salida: " + exitValue);
            System.out.println(System.lineSeparator() + "La ejecución del Random10 se ha guardado en el fichero: " + RANDOM_FILE);
            if (exitValue != 0) {
                System.err.println("ERROR RANDOM10:");
            }

        }
    }
}
